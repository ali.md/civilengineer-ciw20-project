<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>8Behesht</title>
<link href="<?php bloginfo('template_url'); ?>/1styles.css" rel="stylesheet" type="text/css">
<link href="<?php bloginfo('template_url'); ?>/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/script.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slider.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/activemenu.js"></script>
<!--[if IE]>
		<script type="text/javascript" src="js/html5.js"></script>
	<![endif]-->
</head>

<body>
<div class="container">
	<div class="container-header">
    	
		<div class="logo w3 left"></div><!-- end logo-->
        <div class="menu ml5">
			<ul>
				<?php
					wp_list_pages(array(
						'title_li'=>''
						));
				?>
			</ul>
            
		</div><!-- end menu-->
      
        
   	</div><!-- end header-->