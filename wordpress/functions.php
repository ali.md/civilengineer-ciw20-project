<?php
	add_theme_support('menus');
	register_nav_menu('mainmenu', "Main Manu");
	add_theme_support('post-thumbnails');
	
add_action('init', 'product_init');

function product_init (){
	$labels = array(
		'name' => 'محصولات',
		'singular_name' => 'محصولات',
		'add_new' => 'افزودن محصول',
		'add_new_item' => 'افزودن محصول جدید',
		'edit_item' => 'ویرایش محصول',
		'new_item' => 'محصول جدید',
		'view_item' => 'نمایش محصول',
		'search_items' => 'جستجوی محصول',
		'not_found' => 'محصولی یافت نشد',
		'not_found_in_trash' => 'محصولی در زباله دان یافت نشد',
		'menu_name' => 'محصولات'
	);
		
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 20,
		'supports' => array('title','editor','thumbnail')
	);

	register_post_type('product',$args);
	register_taxonomy_for_object_type('category', 'product');
}




add_shortcode('contact','contact_us');

function contact_us(){
	return file_get_contents( get_template_directory() . '/contact.html');
}