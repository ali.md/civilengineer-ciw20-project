<div class="content">
	
	<?php
		if( have_posts() ) {
	?>

		<?php the_post(); 
			$meta = get_post_custom();
		?>
		<article>
			<h1><?php the_title(); ?></h1>
			<p><?php the_content(); ?></p>
            <?php
				if(count($meta['img'])>0){
					echo "<div class='post_images'>";
					foreach ($meta['img'] as $img_id) {
						$img_small = wp_get_attachment_image($img_id,'thumbnail'); 
						echo "<div>$img_small</div>"; 
					}
					echo "</div>";
				}
			?>
		</article>

	<?php
		} else {
	?>
		<article>
			<h1>Page not found!</h1>
		</article>
	<?php
		}
	?>
</div><!-- end content-->
	
