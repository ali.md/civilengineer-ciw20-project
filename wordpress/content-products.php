<div class="content">

	<?php
		if( have_posts() ) {
	?>

		<?php the_post(); 
			$meta = get_post_custom();
		?>
		<article>
			<h1><?php the_title(); ?></h1>
			<p><?php the_content(); ?></p>
            <?php
				if(count($meta['img'])>0){
					echo "<div class='about_image'>";
					foreach ($meta['img'] as $img) {
						echo "<img src='$img' width='223' />";
					}
					echo "</div>";
				}
			?>
            
		</article>

	<?php
		} else {
	?>
    
		<article>
			<h1>Page not found!</h1>
		</article>
	<?php
		}
	?>

	

	
</div>